__copyright__ = "Copyright (C) 2020,2021 Blaise Li"
__licence__ = "GNU GPLv3"
from .bam25prime import (
    # re-export for convenience
    AlignmentFile, BedTool,
    collapse_ali,
    collapse_bed,
    collapse_and_sort,
    collapse_and_sort_bedtool,
    filter_feature_size,
    make_bed_shifter,
    make_bed_shift_checker,
    )

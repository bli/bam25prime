#!/usr/bin/env python3
# Copyright (C) 2020,2021 Blaise Li
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
# vim: set fileencoding=<utf-8> :
"""
When executed, this module collapses reads in a bam file to their 5-prime end
(reducing them to length 1) and writes the result in bed format.
"""

import argparse
# from pybedtools import BedTool, Interval
from pybedtools import BedTool
from pybedtools.featurefuncs import greater_than, less_than
from pysam import AlignmentFile
from .libcollapsesam import collapse_ali
from .libcollapsebed import (
    collapse_bed,
    make_bed_shifter, make_bed_shift_checker)

# pybedtools.Interval | pysam.AlignedSegment
# chrom               | reference_name
# start               | reference_start
# end                 | reference_end
# bool(int(strand))   | is_reverse
#
# Interval(reference_name, reference_start, reference_end, strand=strand)


def filter_feature_size(bedtool, min_len=None, max_len=None):
    """
    Skip features from :class:`pybedtools.BedTool` *bedtool*
    if they are outside the boundaries defined by *min_len* and *max_len*.
    """
    if min_len is not None and min_len >= 1 and max_len is not None:
        return bedtool.filter(greater_than, min_len - 1).filter(
            less_than, max_len + 1)
    if min_len is not None and min_len >= 1:
        return bedtool.filter(greater_than, min_len - 1)
    if max_len is not None:
        return bedtool.filter(less_than, max_len + 1)
    return bedtool


# filename = "../RNA_Seq_analyses/results_44hph_vs_38hph/hisat2/mapped_C_elegans/WT_44hph_4_polyU_on_C_elegans_sorted.bam"
# bamfile = pysam.AlignmentFile(filename, "rb")
# beds_in = pybedtools.BedTool(filename)
#
# In [99]: %timeit [int(bed.strand) for bed in beds_in]
# 2.03 s ± 25.3 ms per loop (mean ± std. dev. of 7 runs, 1 loop each)
#
# In [100]: %timeit [ali.is_reverse for ali in bamfile.fetch()]
# 67 ms ± 1.1 ms per loop (mean ± std. dev. of 7 runs, 10 loops each)
#
# In [101]: beds_in = pybedtools.BedTool(filename)
#
# In [102]: %timeit [bed.strand for bed in beds_in]
# 2.05 s ± 5.3 ms per loop (mean ± std. dev. of 7 runs, 1 loop each)
#
# In [103]: beds_in = pybedtools.BedTool(filename)
#
# In [104]: %timeit [bed.strand == "16" for bed in beds_in]
# 2 s ± 2.52 ms per loop (mean ± std. dev. of 7 runs, 1 loop each)


def collapse_and_sort(alis, shift=0):
    """
    Collapse elements from *alis* to their 5 prime end.

    If *shift* is not zero, coordinates are shifted by *shift*
    positions (with respect to the read orientation).

    The resulting :class:`pybedtools.BedTool` is sorted.

    :param alis: An iterable of :class:`pysam.AlignedSegment`
    :rtype: :class:`pybedtools.BedTool`

    :class:`pybedtools.BedTool` is an iterable:
    It can be iterated over in a for loop, or produce an iterator
    using :func:`iter` on it
    (see https://stackoverflow.com/a/28353158/1878788).
    """
    if shift == 0:
        return BedTool(
            "\n".join(map(collapse_ali, alis)),
            from_string=True).sort(stream=True)
    shift_bed = make_bed_shifter(shift)
    canshift_bed = make_bed_shift_checker(shift)
    return BedTool(
        "\n".join(map(collapse_ali, alis)),
        from_string=True).filter(canshift_bed).each(
            shift_bed).remove_invalid().sort(stream=True)


def collapse_and_sort_bedtool(bedtool, shift=0):
    """
    Collapse elements from *bedtool* to their 5 prime end.

    If *shift* is not zero, coordinates are shifted by *shift*
    positions (with respect to the feature's orientation).

    The resulting :class:`pybedtool.BedTool` is sorted.

    :param bedtool: A :class:`pybedtool.BedTool`
    :rtype: :class:`pybedtool.BedTool`

    :class:`pybedtool.BedTool` is an iterable:
    It can be iterated over in a for loop, or produce an iterator
    using :func:`iter` on it
    (see https://stackoverflow.com/a/28353158/1878788).
    """
    if shift == 0:
        return bedtool.each(collapse_bed).sort(stream=True)
    shift_bed = make_bed_shifter(shift)
    canshift_bed = make_bed_shift_checker(shift)
    return bedtool.each(collapse_bed).filter(canshift_bed).each(
        shift_bed).remove_invalid().sort(stream=True)


def main() -> int:
    """Main function of the program."""
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument(
        "-b", "--bamfile",
        required=True,
        help="Sorted and indexed bam file containing the aligned reads.")
    parser.add_argument(
        "-o", "--out_bed",
        required=True,
        help="Bed file in which the results should be written.")
    args = parser.parse_args()
    with AlignmentFile(args.bamfile, "rb") as bamfile:
        collapse_and_sort(bamfile.fetch()).saveas(args.out_bed)
    return 0


if __name__ == "__main__":
    import sys
    sys.exit(main())

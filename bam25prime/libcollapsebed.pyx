#!/usr/bin/env python3
# Copyright (C) 2020,2021 Blaise Li
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# cython: language_level=3
"""
This library contains a cythonized version of a function to collapse
:class:`pybedtools.Interval`s to their 5' end.
"""

# http://stackoverflow.com/a/23575771/1878788
# Also works by setting language_level as comment
# at the top of the file.
#from __future__ import print_function
from pybedtools.cbedtools cimport Interval


cdef ccanshift_bed(Interval bed, int shift):
    """
    Check whether the *bed* Interval could be shifted by
    *shift* positions without having negative coordinates.

    This should be used as filter before attempting shifts,
    in order to avoid invalid intervals.
    """
    if bed.strand == "-":
        if shift > bed.start:
            return False
        # bed.start = bed.start - shift  # would be negative
        # bed.stop = bed.stop - shift
    else:
        if (-shift) > bed.start:
            return False
        # bed.start = bed.start + shift  # would be negative
        # bed.stop = bed.stop + shift
    return True


def make_bed_shift_checker(shift):
    """
    Make a function that checks whether bed intervals
    can be shifted by *shift* positions (with respect to
    the feature's orientation).
    """
    def canshift_bed(bed):
        """
        Check whether the *bed* Interval could be shifted by
        *shift* positions without having negative coordinates.

        This should be used as filter before attempting shifts,
        in order to avoid invalid intervals.
        """
        return ccanshift_bed(bed, shift)
    return canshift_bed


cdef cshift_bed(Interval bed, int shift):
    """
    Return the Interval corresponding to the shift
    of *shift* positions of Interval *bed*.
    """
    if bed.strand == "-":
        bed.start = bed.start - shift
        bed.stop = bed.stop - shift
    else:
        bed.start = bed.start + shift
        bed.stop = bed.stop + shift
    return bed


def make_bed_shifter(shift):
    """
    Make a function that shifts bed intervals by *shift*
    positions (with respect to the feature's orientation).
    """
    def shift_bed(bed):
        """
        Return the Interval corresponding to the shift
        of *shift* positions of Interval *bed*.
        """
        return cshift_bed(bed, shift)
    return shift_bed


cdef ccollapse_bed(Interval bed):
    """
    Return the Interval corresponding to the 5' collapse of Interval *bed*.
    """
    if bed.strand == "-":
        bed.start = bed.stop - 1
    else:
        bed.stop = bed.start + 1
    return bed


def collapse_bed(bed):
    """
    Return the Interval corresponding to the 5' collapse of Interval *bed*.
    """
    return ccollapse_bed(bed)


def main():
    """Example use case."""
    print("No examples to show here.")
    return 0


if __name__ == "__main__":
    import sys
    sys.exit(main())

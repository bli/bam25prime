#!/usr/bin/env python3
"""
Test cases for bam25prime.
"""

import unittest
from collections import namedtuple
from pathlib import Path
from bam25prime import (
    BedTool, make_bed_shifter, make_bed_shift_checker)


Interval = namedtuple("Interval", ["chrom", "start", "stop", "strand"])

THIS_DIR = Path("__file__").resolve().parent
data_dir = THIS_DIR.joinpath("data")


def load_interval_dict(bed_path):
    """
    Build a dict to store intervals allowing easy comparison between
    obtained and expected results using bed names (4th column) as keys.
    """
    itvl_dict = {}
    with open(bed_path) as fh:
        for line in fh:
            if len(line.strip()) == 0 or line[0] == "#":
                continue
            (
                chrom, start, stop,
                name, _, strand, *_) = line.strip().split("\t")
            assert name not in itvl_dict, f"Non unique name: {name}"
            itvl_dict[name] = Interval(
                chrom, int(start), int(stop), strand)
    return itvl_dict


def compare_itvl_dicts(observed, expected):
    """
    Compute the sets of interval names:
    * missing in the observed
    * spurious in the observed
    * different in the observed
    Return a (missing, spurious, different) tuple.
    """
    obs_names = set(observed.keys())
    exp_names = set(expected.keys())
    missing = exp_names - obs_names
    spurious = obs_names - exp_names
    common = obs_names & exp_names
    different = set()
    for itvl_name in common:
        if observed[itvl_name] != expected[itvl_name]:
            different.add(itvl_name)
    return (missing, spurious, different)


def check_shifter(bed_path, s_name, shifter, checker):
    bt = BedTool(bed_path)
    bed_name = bed_path.stem
    msg_base = f"{bed_name} transformed with {s_name}"
    print(msg_base)
    expected = load_interval_dict(
        data_dir.joinpath("shifted", s_name, f"{bed_name}.bed"))
    #print("bt")
    #print(bt)
    #print(bt)
    #shifted_1 = bt.each(shifter).saveas()
    #print("  shifted")
    #print(shifted_1)
    #print(shifted_1)
    #shifted_2 = shifted_1.remove_invalid().saveas()
    shifted_2 = bt.filter(checker).each(shifter).remove_invalid().saveas()
    #print(" invalid removed")
    #print(shifted_2)
    #print(shifted_2)
    shifted = shifted_2.sort()
    #print("  sorted")
    #print(shifted)
    #print(shifted)
    observed = load_interval_dict(shifted.fn)
    #print(observed)
    (missing, spurious, different) = compare_itvl_dicts(
        observed, expected)
    msg = "\n".join([
        msg_base,
        f"\tMissing: {missing}",
        f"\tSpurious: {spurious}",
        f"\tDifferent: {different}"])
    return (missing, spurious, different, msg)


class TestBam25prime(unittest.TestCase):
    def test_shift_pos(self):
        shifters = {
            f"p{shift}": (
                make_bed_shifter(shift),
                make_bed_shift_checker(shift))
            for shift in [0, 1, 4, 5, 6, 9, 10, 11, 19, 20, 21]
        }
        for bed_path in data_dir.glob("*.bed"):
            for (s_name, (shifter, checker)) in shifters.items():
                (missing, spurious, different, msg) = check_shifter(
                    bed_path, s_name, shifter, checker)
                self.assertFalse(
                    missing | spurious | different,
                    msg)

    def test_shift_neg(self):
        shifters = {
            f"m{shift}": (
                make_bed_shifter(-shift),
                make_bed_shift_checker(-shift))
            for shift in [0, 1, 4, 5, 6, 9, 10, 11, 19, 20, 21]
        }
        for bed_path in data_dir.glob("*.bed"):
            for (s_name, (shifter, checker)) in shifters.items():
                (missing, spurious, different, msg) = check_shifter(
                    bed_path, s_name, shifter, checker)
                self.assertFalse(
                    missing | spurious | different,
                    msg)


if __name__ == "__main__":
    unittest.main()

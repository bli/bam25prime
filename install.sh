#!/bin/sh
#/usr/bin/env python3 setup.py build_ext
# .egg-link does not work with PYTHONPATH ?
# Install custom pybedtools first
/usr/bin/env python3 -m pip install -r requirements.txt
#/usr/bin/env python3 -m pip install --no-deps --global-option="cythonize" git+https://github.com/blaiseli/pybedtools.git@fix_missing_headers
/usr/bin/env python3 -m pip install --no-build-isolation -e .
/usr/bin/env python3 -m pip install --no-build-isolation --no-deps --ignore-installed .
#/usr/bin/env python3 -m pip install -e .
#/usr/bin/env python3 -m pip install --no-deps --ignore-installed .

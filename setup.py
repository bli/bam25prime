# Copyright (C) 2020 Blaise Li
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
from os import environ, PathLike
from setuptools import setup, find_packages
# from glob import glob
# If you have .pyx things to cythonize
from distutils.extension import Extension
#from Cython.Build import cythonize
# https://github.com/daler/pybedtools/issues/253
from pybedtools.helpers import get_includes as pybedtools_get_includes
from pysam import get_include as pysam_get_include

name = "bam25prime"
__version__ = "0.3"


# https://stackoverflow.com/a/54138355/1878788
# defer the import of these modules after setup_requires have been provided
#class pysam_get_includes(str):
#    def __str__(self):
#        from pysam import get_include
#        includes = get_include()
#        print(*includes, sep="\n")
#        return "|".join(includes)
#
#class pybedtools_get_includes(str):
#    def __str__(self):
#        from pybedtools.helpers import get_includes
#        includes = get_includes()
#        print(*includes, sep="\n")
#        return "|".join(includes)


# https://github.com/cython/cython/blob/master/docs/src/reference/compilation.rst#configuring-the-c-build
extensions = [
    Extension(
        "bam25prime.libcollapsesam", ["bam25prime/libcollapsesam.pyx"],
        #include_dirs=pysam_get_includes().split("|")),
        include_dirs=pysam_get_include()),
    Extension(
        "bam25prime.libcollapsebed", ["bam25prime/libcollapsebed.pyx"],
        #include_dirs=glob("%s/src/bedtools2-2.27.1/src/utils/*" % environ["HOME"]),
        #include_dirs=pybedtools_get_includes().split("|"),
        include_dirs=pybedtools_get_includes(),
        language="c++"),
    ]

setup(
    name=name,
    version=__version__,
    description="Library providing utilities to collapse aligned segments"
                "in a bam file to their 5-prime end.",
    author="Blaise Li",
    author_email="blaise.li@normalesup.org",
    license="GNU GPLv3",
    package_data={"bam25prime": ["py.typed", "bam25prime.pyi"]},
    packages=find_packages(),
    zip_safe=False,
    entry_points={
        "console_scripts": ["bam25prime = bam25prime.bam25prime:main",]
    },
    # scripts=["bin/%s" % name],
    #ext_modules = cythonize(extensions),
    ext_modules = extensions,
    #ext_modules = cythonize("bam25prime/libbam25prime.pyx", include_path=pysam_get_include()),
    setup_requires=[
        "wheel",
        "cython",
    ],
    install_requires=[
        "pysam>=0.16",
        "pybedtools @ git+https://github.com/blaiseli/pybedtools.git@issue-343",
        # "pybedtools",
    ]
)
# If you have .pyx things to cythonize
    #ext_modules = cythonize("libsmallrna/libsmallrna.pyx"),
    #install_requires=["cytoolz"],
    #zip_safe=False)

# Utilities to collapse reads in bam files to their 5' end.


## Installing

Get the source using `git clone git@gitlab.pasteur.fr:bli/bam25prime.git`, `cd` into it and run `python3 -m pip install .`

It might also work directly:

    python3 -m pip install git+ssh://git@gitlab.pasteur.fr/bli/bam25prime.git


### Troubleshooting

Compilation may fail if the version of `gcc` is too old.

On a cluster with environment modules, a workaround might be something like running `module load gcc/7.2.0`.

You may encounter the following error:

    ImportError: liblzma.so.5: cannot open shared object file: No such file or directory

You need to locate this `liblzma.so.5` file. If you find it, then set the
`LD_LIBRARY_PATH` environment variable so that it points to the directory
containing it. For instance:

    export LD_LIBRARY_PATH="/local/gensoft2/exe/xz/5.2.2/lib"



## Usage

This package provides a `bam25prime` script.
Its help can be obtained using option `-h` or `--help`.

```
$ bam25prime --help
usage: bam25prime [-h] -b BAMFILE -o OUT_BED

When executed, this module collapses reads in a bam file to their 5-prime end
(reducing them to length 1) and writes the result in bed format.

optional arguments:
  -h, --help            show this help message and exit
  -b BAMFILE, --bamfile BAMFILE
                        Sorted and indexed bam file containing the aligned
                        reads. (default: None)
  -o OUT_BED, --out_bed OUT_BED
                        Bed file in which the results should be written.
                        (default: None)
```
